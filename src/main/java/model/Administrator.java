package model;

public class Administrator {

    private int admin_id;
    private String a_name;
    private String a_surname;

    public int getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public String getA_name() {
        return a_name;
    }

    public void setA_name(String a_name) {
        this.a_name = a_name;
    }

    public String getA_surname() {
        return a_surname;
    }

    public void setA_surname(String a_surname) {
        this.a_surname = a_surname;
    }
}
