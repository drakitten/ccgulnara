import config.ApplicationConfig;
import model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import repository.CustomerRepository;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        Customer customer = new Customer();
        customer.setCust_id(9);
        customer.setC_name("Karen");
        customer.setC_surname("Johnson");
        customer.setAddress_id(1);

        CustomerRepository repository = context.getBean(CustomerRepository.class);

        repository.addCustomer(customer);
        repository.deleteCustomer(9);


        context.close();
    }
}
