package repository;

import model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CustomerRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public CustomerRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public void addCustomer(Customer customer) {
        String sql = "INSERT INTO customer VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                            customer.getCust_id(),
                            customer.getC_name(),
                            customer.getC_surname(),
                            customer.getAddress_id());

    }

    @Transactional
    public void deleteCustomer(int id) {
        String sql = "DELETE FROM customer WHERE cust_id=?";
        jdbcTemplate.update(sql, id);
    }

    @Transactional
    public Customer getCustomerById(int id) {
        String sql = "SELECT c_name FROM customer WHERE cust_id="+id;
        return null;
    }

    @Transactional
    public List<Customer> getAllCustomers(){
    String sql = "SELECT * FROM customer";
    return jdbcTemplate.query(sql, new RowMapper<Customer>() {
        @Override
        public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
            Customer customer = new Customer();
            customer.setCust_id(resultSet.getInt("cust_id"));
            customer.setC_name(resultSet.getString("c_name"));
            customer.setC_surname(resultSet.getString("c_surname"));
            customer.setAddress_id(resultSet.getInt("address_id"));
            return customer;
        }
    });
}

}
