package repository;

import model.Administrator;
import model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AdministratorRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public AdministratorRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public void addAdministrator(Administrator administrator) {
        String sql = "INSERT INTO administrator VALUES (?, ?, ?)";
        jdbcTemplate.update(sql,
                administrator.getAdmin_id(),
                administrator.getA_name(),
                administrator.getA_surname());
    }

    @Transactional
    public void deleteService(int id) {
        String sql = "DELETE FROM administrator WHERE id=?";
        jdbcTemplate.update(sql, id);
    }
}
