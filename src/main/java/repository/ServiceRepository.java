package repository;

import model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ServiceRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ServiceRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional
    public void addService(Service service) {
        String sql = "INSERT INTO service VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                                service.getService_id(),
                                service.getS_name(),
                                service.getDuration_hours(),
                                service.getCh_group_id(),
                                service.getTool_group_id());
    }

    @Transactional
    public void deleteService(int id) {
        String sql = "DELETE FROM service WHERE id=?";
        jdbcTemplate.update(sql, id);
    }
}
